var gulp           = require('gulp'),
    sitemap        = require('gulp-sitemap'),
    robots         = require('gulp-robots');

	
gulp.task('sitemap', function () {
    gulp.src('dist/**/*.html', {
            read: false
        })
        .pipe(sitemap({
            siteUrl: 'http://site'
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('robots', function () {
    gulp.src('dist/index.html')
        .pipe(robots({
            useragent: 'Googlebot, Yandex ',
            allow: ['dist/ '],
            disallow: ['app/ ']
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('meta', ['sitemap', 'robots' ]);