var gulp           = require('gulp'),
	del            = require('del'),
    uglify         = require('gulp-uglifyjs'),
	tinypng        = require('gulp-tinypng-compress'),
	htmlmin        = require('gulp-htmlmin'),
	gcmq           = require('gulp-group-css-media-queries'),
	cleancss       = require('gulp-clean-css'),
	penthouse      = require('penthouse'),
    inject         = require('gulp-inject-string');


gulp.task('penthouse', ['prebuild'], function () {
	penthouse({
	  url: 'dist/index.html',
	  css: 'dist/css/main.css',
	  width: 1280,
	  height: 800
	}, function (err, criticalCss) {
	gulp.src('dist/index.html')
	  .pipe(inject.after('\n<style>\n' + criticalCss + '\n</style>'))
	  .pipe(gulp.dest('dist'))
	});
});

gulp.task('clean', function() {
	return del.sync('dist');
});

gulp.task('prebuild', /* ['clean',], */ function() {

	var buildCss = gulp.src('app/css/**/*')
	.pipe(cleancss({
		level: { 2: { specialComments: 0 },
		compatibility: 'ie9'
	}}))
	.pipe(gcmq())
	.pipe(gulp.dest('dist/css'))

    var buildFont = gulp.src('app/font/**/*')
	.pipe(gulp.dest('dist/font'))
    
	var buildJs = gulp.src('app/js/*')
    .pipe(uglify())
	.pipe(gulp.dest('dist/js'))
	
	var buildImg = gulp.src('app/img/**/*')
	.pipe(tinypng({
		key: 'khxIPqiJ0lt8o3IIY3Y5jdV2mo38Kiu6',
		sigFile: 'app/img/.tinypng-sigs',
		log: true
	}))
	.pipe(gulp.dest('dist/img'))

	var buildHtml = gulp.src('app/*.html')
	.pipe(htmlmin({collapseWhitespace: true}))
	.pipe(gulp.dest('dist'));
});

gulp.task('build', ['penthouse'], function(){});

/* Сборка без критических стилей */
// gulp.task('build', ['prebuild'], function(){});
