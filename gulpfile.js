var gulp           = require('gulp'),
    sass           = require('gulp-sass'),
	autoprefixer   = require('gulp-autoprefixer'),
    nunjucks       = require('gulp-nunjucks-render'),
    useref         = require('gulp-useref'),
    gulpif         = require('gulp-if'),
    babel          = require('gulp-babel'),
    browserSync    = require('browser-sync'),
    notify         = require('gulp-notify'),
    requireDir     = require('require-dir');


gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false,
		// open: false,
		// online: false, // Work Offline Without Internet Connection
		// tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
	})
});

gulp.task('scss', function(){
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass()
        .on('error', notify.onError(function (error) {
            return "A task sсss error occurred: " + error.message;
        })))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(gulp.dest('app/css'))
});

gulp.task('nunjucks', function(){
    return gulp.src(['app/nunjucks/**/*.html', '!app/nunjucks/**/_*.html']) 
        .pipe(nunjucks({
            path: ['app/nunjucks/']
        })).on('error', notify.onError(function (error) {
            return "A task nunjucks error occurred: " + error.message;
        }))
        .pipe(gulp.dest('app/'))
});

gulp.task('useref', ['nunjucks'], function(){
    return gulp.src('app/*.html') 
        .pipe(useref()).on('error', notify.onError(function (error) {
            return "Useref error occurred: " + error.message;
        }))
        .pipe(gulpif('_*.js', babel({
            presets: ['@babel/env']
        })))
        .pipe(gulp.dest('app/'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('watch', ['browser-sync', 'scss', 'useref'], function() {
	gulp.watch('app/scss/**/*.scss', ['scss']);
    gulp.watch('app/nunjucks/**/*.html', ['useref']);
	gulp.watch('app/js/**/*.js', browserSync.reload);
	gulp.watch('app/css/**/*.css', browserSync.reload);
});

gulp.task('default', ['watch'], function(){});

/* 
Сборка проекта (минификация, оптимизация)
npm install del gulp-uglifyjs gulp-tinypng-compress gulp-htmlmin gulp-group-css-media-queries gulp-clean-css penthouse gulp-inject-string
*/
// var dir = requireDir('gulp_modules/build');  

/*
Молниеностная выгрузка на сервер
npm install gulp-rsync
*/
// var dir = requireDir('gulp_modules/rsync');  

/*
Генерация robots.txt и sitemap
npm install gulp-sitemap gulp-robots
*/
// var dir = requireDir('gulp_modules/sitemap');

/*
Выгрузка файлов на сервер по ftp
npm install vinyl-ftp
*/
// var dir = requireDir('gulp_modules/ftp');  